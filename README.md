# git clone all packages
git clone --recursive https://git.hyperbola.info:50100/packages/packages.git

# manualy git clone all packages
1 - git clone https://git.hyperbola.info:50100/packages/packages.git
2 - cd packages
3 - git submodule init
4 - git submodule update

# to add git submodule
git submodule add git clone https://git.hyperbola.info:50100/packages/xxx.git

# see: "man git-submodules" and "https://git.wiki.kernel.org/index.php/GitSubmoduleTutorial"

# go to the master branch in all repos (core, extra, community and multilib) before pull/push changes
1 - cd $repo
2 - git checkout master
